package com.example.psyke.lecteurmusique.modele;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by psyke on 11/11/2016.
 */

public class Song implements Parcelable
{
    private long id;
    private String title;
    private String artist;
    private String album;
    private Uri uri;
    public Song(long songID, String songTitle, String album, String songArtist, Uri uri) {
        id = songID;
        title = songTitle;
        artist = songArtist;
        this.uri = uri;
        this.album = album;
    }

    protected Song(Parcel in) {
        id = in.readLong();
        title = in.readString();
        artist = in.readString();
        album = in.readString();
        uri = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    public long getID(){return id;}
    public String getTitle(){return title;}
    public String getArtist(){return artist;}
    public Uri getURI(){return uri;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(album);
        dest.writeParcelable(uri, flags);
    }
    public boolean isEqual (Song song){
        if (id == song.id)
            return true;
        else
            return false;
    }
}
