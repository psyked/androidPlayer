package com.example.psyke.lecteurmusique;

import android.app.Application;
import android.content.Context;

/**
 * Created by arnau on 16/11/2016.
 */

public class androidPlayer extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        androidPlayer.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return androidPlayer.context;
    }
}
