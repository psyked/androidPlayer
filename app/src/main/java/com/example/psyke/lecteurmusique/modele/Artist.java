package com.example.psyke.lecteurmusique.modele;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by arnau on 16/11/2016.
 */

public class Artist implements Parcelable {

    String name;
    ArrayList<Album> albums= new ArrayList<Album>();
    int totalSongsNumber,AlbumsNumber;
    Album all;


    public Artist(String name) {
        this.name = name;
        setAlbumsNumber(-1);
        setTotalSongsNumber(0);
        all = new Album(this,"All");
        addAlbum(all);
    }

    public int getTotalSongsNumber() {
        return totalSongsNumber;
    }

    public void setTotalSongsNumber(int totalSongsNumber) {
        this.totalSongsNumber = totalSongsNumber;
    }

    public int getAlbumsNumber() {
        return AlbumsNumber;
    }

    public void setAlbumsNumber(int albumsNumber) {
        AlbumsNumber = albumsNumber;
    }

    protected Artist(Parcel in) {
        name = in.readString();
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }


    public void addAlbum(Album album){
        albums.add(album);
        setAlbumsNumber(getAlbumsNumber()+1);
        setTotalSongsNumber(getTotalSongsNumber()+album.getSongsNumber());
        all.addSongs((ArrayList<Song>) album.getSongs());

    }

    public void removeAlbum(Album album){
        albums.remove(album);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(name);
    }
}
