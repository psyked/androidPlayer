package com.example.psyke.lecteurmusique.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psyke.lecteurmusique.modele.Album;
import com.example.psyke.lecteurmusique.DataColector;
import com.example.psyke.lecteurmusique.fragment.DisplayListFragment.OnListFragmentInteractionListener;
import com.example.psyke.lecteurmusique.R;

import java.util.ArrayList;
import java.util.List;


public class MyAlbumRecyclerViewAdapter extends RecyclerView.Adapter<MyAlbumRecyclerViewAdapter.ViewHolder> {

    private final DataColector dataColector= new DataColector();
    private final List<Album> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyAlbumRecyclerViewAdapter(OnListFragmentInteractionListener listener, ArrayList<Album> list) {
        if (list == null)
            mValues = dataColector.getAlbumList();
        else
            mValues = list;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_album, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String subtitle = mValues.get(position).getSongsNumber()+" morceaux -"+mValues.get(position).getArtist().getName();
        holder.mAlbum = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mContentView.setText(subtitle);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mAlbum);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Album mAlbum;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.textView2);
            mContentView = (TextView) view.findViewById(R.id.textView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
