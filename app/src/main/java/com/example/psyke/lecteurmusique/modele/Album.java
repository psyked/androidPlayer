package com.example.psyke.lecteurmusique.modele;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arnau on 16/11/2016.
 */

public class Album implements Parcelable{
    List<Song> songs = new ArrayList<Song>();
    Artist artist;
    String name;
    int songsNumber;

    public Album(Artist artist, String name) {

        this.artist = artist;
        this.name = name;
        this.setSongsNumber(0);

    }

    public int getSongsNumber() {
        return songsNumber;
    }

    public void setSongsNumber(int songsNumber) {
        this.songsNumber = songsNumber;
    }

    protected Album(Parcel in) {
        artist = in.readParcelable(Artist.class.getClassLoader());
        name = in.readString();
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public List<Song> getSongs() {

        return songs;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void addSong(Song song){
        songs.add(song);
        setSongsNumber(getSongsNumber()+1);
    }

    public void addSongs(ArrayList<Song> songs){
        this.songs.addAll(songs);
        setSongsNumber(getSongsNumber()+songs.size());
    }

    public void removeSong(Song song){
        songs.remove(song);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(songs);
        dest.writeParcelable(artist, flags);
        dest.writeString(name);
    }
}
