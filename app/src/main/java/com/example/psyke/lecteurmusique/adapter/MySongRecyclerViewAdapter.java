package com.example.psyke.lecteurmusique.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psyke.lecteurmusique.DataColector;
import com.example.psyke.lecteurmusique.R;
import com.example.psyke.lecteurmusique.modele.Song;
import com.example.psyke.lecteurmusique.fragment.DisplayListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arnau on 17/11/2016.
 */

public class MySongRecyclerViewAdapter extends RecyclerView.Adapter<MySongRecyclerViewAdapter.ViewHolder> {

    private DataColector dataColector= new DataColector();
    private final List<Song> mValues;
    private final DisplayListFragment.OnListFragmentInteractionListener mListener;

    public MySongRecyclerViewAdapter(DisplayListFragment.OnListFragmentInteractionListener listener, ArrayList<Song> list) {
        if(list == null) {
            mValues = dataColector.getSongList();
        }
        else

            mValues = list;
        mListener = listener;
    }

    @Override
    public MySongRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.song, parent, false);
        return new MySongRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MySongRecyclerViewAdapter.ViewHolder holder, int position) {
        holder.mSong = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getTitle());
        holder.mContentView.setText(mValues.get(position).getArtist());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mSong);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Song mSong;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.textView2);
            mContentView = (TextView) view.findViewById(R.id.textView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
