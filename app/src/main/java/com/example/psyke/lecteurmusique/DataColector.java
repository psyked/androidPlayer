package com.example.psyke.lecteurmusique;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.example.psyke.lecteurmusique.modele.Album;
import com.example.psyke.lecteurmusique.modele.Artist;
import com.example.psyke.lecteurmusique.modele.Song;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by arnau on 16/11/2016.
 */

public class DataColector
{
    private Map<String,Map<String,ArrayList<Song>>> artists = new HashMap<String,Map<String,ArrayList<Song>>>();
    private ArrayList<Artist> artistList;
    private ArrayList<Album> albumList;
    private ArrayList<Song> songList;


    public DataColector (){
        artistList = new ArrayList<Artist>();
        albumList = new ArrayList<Album>();
        songList = new ArrayList<Song>();
        String a ="";

        initialiaseList();

    }

    public void getMap() {
        //retrieve song info

        ContentResolver musicResolver = androidPlayer.getAppContext().getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int dataColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.DATA);
            int albumColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ALBUM);
            //add songs to list
            do {
                if (artists.containsKey(musicCursor.getString(artistColumn))){
                    if(artists.get(musicCursor.getString(artistColumn)).containsKey(musicCursor.getString(albumColumn))){
                        artists.get(musicCursor.getString(artistColumn)).get(musicCursor.getString(albumColumn)).add(
                                new Song(musicCursor.getLong(idColumn), musicCursor.getString(titleColumn), musicCursor.getString(albumColumn), musicCursor.getString(artistColumn), Uri.parse("file:///"+musicCursor.getString(dataColumn)))
                        );
                    }
                    else{
                        artists.get(musicCursor.getString(artistColumn)).put(musicCursor.getString(albumColumn),new ArrayList<Song>());
                        artists.get(musicCursor.getString(artistColumn)).get(musicCursor.getString(albumColumn)).add(
                                new Song(musicCursor.getLong(idColumn), musicCursor.getString(titleColumn), musicCursor.getString(albumColumn), musicCursor.getString(artistColumn), Uri.parse("file:///"+musicCursor.getString(dataColumn)))
                        );
                    }
                }
                else{
                    artists.put(musicCursor.getString(artistColumn),new HashMap<String, ArrayList<Song>>());
                    artists.get(musicCursor.getString(artistColumn)).put(musicCursor.getString(albumColumn),new ArrayList<Song>());
                    artists.get(musicCursor.getString(artistColumn)).get(musicCursor.getString(albumColumn)).add(
                            new Song(musicCursor.getLong(idColumn), musicCursor.getString(titleColumn), musicCursor.getString(albumColumn), musicCursor.getString(artistColumn), Uri.parse("file:///"+musicCursor.getString(dataColumn)))
                    );
                }
            }
            while (musicCursor.moveToNext());
        }
    }
    public void initialiaseList(){
        getMap();
        for (Map.Entry<String,Map<String,ArrayList<Song>>> album: artists.entrySet()) {
            Artist artist = new Artist(album.getKey());
            artistList.add(artist);
            for (Map.Entry<String,ArrayList<Song>> titre: album.getValue().entrySet()){
                Album currentAlbum = new Album(artist,titre.getKey());
                currentAlbum.addSongs(titre.getValue());
                artist.addAlbum(currentAlbum);
                albumList.add(artist.getAlbums().get(artist.getAlbums().size()-1));
                songList.addAll(currentAlbum.getSongs());
            }
        }
    }


    public ArrayList<Artist> getArtistList() {
        return artistList;
    }

    public void setArtistList(ArrayList<Artist> artistList) {
        this.artistList = artistList;
    }

    public ArrayList<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(ArrayList<Album> albumList) {
        this.albumList = albumList;
    }

    public ArrayList<Song> getSongList() {
        return songList;
    }

    public void setSongList(ArrayList<Song> songList) {
        this.songList = songList;
    }
}
