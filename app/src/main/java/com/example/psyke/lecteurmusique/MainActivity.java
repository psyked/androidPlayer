package com.example.psyke.lecteurmusique;

import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.net.Uri;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.psyke.lecteurmusique.adapter.SampleFragmentPagerAdapter;
import com.example.psyke.lecteurmusique.fragment.DisplayListFragment;
import com.example.psyke.lecteurmusique.modele.Album;
import com.example.psyke.lecteurmusique.modele.Artist;
import com.example.psyke.lecteurmusique.modele.Playlist;
import com.example.psyke.lecteurmusique.modele.Song;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DisplayListFragment.OnListFragmentInteractionListener{

    private Object selectedItem = null;
    private List<Playlist> playlistList;
    private Playlist currentPlaylist = new Playlist("default",this);
    DataColector dataColector = new DataColector();
    private Album currentAlbum = null;
    private Artist currentArtist = null;
    private int currentIndex = 0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Mes musiques");

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
            currentPlaylist.addSongs(dataColector.getSongList());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(currentArtist != null) {
            if (currentAlbum == null) {
                findViewById(R.id.sliding_tabs).setVisibility(View.VISIBLE);
                setTitle("Mes musiques");
                currentArtist = null;
            }
            else
                setTitle(currentArtist.getName());
            currentAlbum = null;
        }
        else{
            if (currentAlbum != null){
                findViewById(R.id.sliding_tabs).setVisibility(View.VISIBLE);
                setTitle("Mes musiques");
                currentAlbum = null;
            }
        }

    }

    @Override
    public void onListFragmentInteraction(Object o) {

            if (o.getClass() == Song.class) {
                Song song = (Song) o;
                Uri uri = song.getURI();



                if(currentAlbum == null && currentArtist == null) {
                    currentPlaylist.removeAllSongs();
                    currentPlaylist.addSongs(dataColector.getSongList());
                }
                currentIndex = currentPlaylist.getIndexOf(song);

                if(currentPlaylist.getCurrentSong() != null){
                    findViewById(R.id.button_play).setVisibility(View.GONE);
                    findViewById(R.id.button_stop).setVisibility(View.VISIBLE);
                }

                currentPlaylist.stopSong();
                currentPlaylist.playSong(currentIndex);
                TextView textView = (TextView) findViewById(R.id.currentSong);
                textView.setText(currentPlaylist.getSong(currentIndex).getTitle());

                findViewById(R.id.button_previous_song).setVisibility(View.VISIBLE);
                findViewById(R.id.button_n).setVisibility(View.VISIBLE);



            }

            if(o.getClass() == Album.class) {
                currentAlbum = (Album) o;
                selectedItem = currentAlbum;
                findViewById(R.id.sliding_tabs).setVisibility(View.GONE);
                setTitle(currentAlbum.getName() + " by "+currentAlbum.getArtist().getName());
                ArrayList<Parcelable> listToSend = new ArrayList<Parcelable>();
                for (Song song:currentAlbum.getSongs()){
                   listToSend.add(song);
                }
                    currentPlaylist.removeAllSongs();
                    currentPlaylist.addSongs(currentAlbum.getSongs());
                final DisplayListFragment displayListFragment = DisplayListFragment.newInstance(0,3,listToSend);
                this.getSupportFragmentManager().beginTransaction().replace(R.id.Container, displayListFragment, null).addToBackStack(null).commit();


// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack if needed
            }
            if (o.getClass() == Artist.class){
                 currentArtist = (Artist) o;
                selectedItem = currentArtist;
                setTitle(currentArtist.getName());
                ArrayList<Parcelable> listToSend = new ArrayList<Parcelable>();
                for (Album album:currentArtist.getAlbums()){
                    listToSend.add(album);
                }
                final DisplayListFragment displayListFragment = DisplayListFragment.newInstance(0,4,listToSend);
                this.getSupportFragmentManager().beginTransaction().replace(R.id.Container, displayListFragment, "Artist").addToBackStack(null).commit();

                findViewById(R.id.sliding_tabs).setVisibility(View.GONE);

            }
    }
    public void pauseClick(View v)
    {
        currentPlaylist.pauseSong();
        findViewById(R.id.button_play).setVisibility(View.VISIBLE);
        findViewById(R.id.button_stop).setVisibility(View.GONE);
        findViewById(R.id.button_previous_song).setVisibility(View.INVISIBLE);

        findViewById(R.id.button_n).setVisibility(View.INVISIBLE);
    }
    public void playClick(View v)
    {
        currentPlaylist.playSong();
        findViewById(R.id.button_play).setVisibility(View.GONE);
        findViewById(R.id.button_stop).setVisibility(View.VISIBLE);
        TextView textView = (TextView) findViewById(R.id.currentSong);
        textView.setText(currentPlaylist.getSong(currentIndex).getTitle());
        findViewById(R.id.button_previous_song).setVisibility(View.VISIBLE);
        findViewById(R.id.button_n).setVisibility(View.VISIBLE);


    }
    public void previousSongClick(View v)
    {
        currentPlaylist.readPrevious();
        TextView textView = (TextView) findViewById(R.id.currentSong);
        textView.setText(currentPlaylist.getSong(currentPlaylist.index()).getTitle());
    }
    public void nextSongClick(View v)
    {
        currentPlaylist.readNext();
        TextView textView = (TextView) findViewById(R.id.currentSong);
        textView.setText(currentPlaylist.getSong(currentPlaylist.index()).getTitle());

    }
    public void shuffleClick(View v)
    {
        if(((CheckBox)(v)).isChecked())
        {
            currentPlaylist.randomiseSongs();
        }
        else
        {
            currentPlaylist.unRandomiseSongs();
        }

    }
    public void shareClick(View v)
    {
        String strToShare = R.string.is_listening+ " : " +currentPlaylist.getSong(currentPlaylist.index()).getTitle();
        //intendshare
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        Song song = currentPlaylist.getSong(currentPlaylist.index());
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.is_listening) + strToShare +" de "+song.getArtist() );
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }
    public void deleteClick(View v)
    {
        currentPlaylist.delSong(currentPlaylist.getCurrentIndex());
    }
    public void addClick(View v)
    {
        Song songToAdd = currentPlaylist.getCurrentSong();
        //fragment to add a song to a playlist
    }
}
