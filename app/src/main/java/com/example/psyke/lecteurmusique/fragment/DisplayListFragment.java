package com.example.psyke.lecteurmusique.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.psyke.lecteurmusique.modele.Album;
import com.example.psyke.lecteurmusique.DataColector;
import com.example.psyke.lecteurmusique.modele.Song;
import com.example.psyke.lecteurmusique.adapter.MyAlbumRecyclerViewAdapter;
import com.example.psyke.lecteurmusique.R;
import com.example.psyke.lecteurmusique.adapter.MyArtistRecyclerViewAdapter;
import com.example.psyke.lecteurmusique.adapter.MySongRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class DisplayListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String type = "type";
    private static final String ARG_LIST_TO_DISPLAY = "list-display";

    // TODO: Customize parameters
    private int listType = 0;
    private int mColumnCount = 1;
    private ArrayList specificListToDisplay = null;
    private OnListFragmentInteractionListener mListener;
    private DataColector dataColector = new DataColector();
    private Parcelable object;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DisplayListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static DisplayListFragment newInstance(int columnCount, int listType, ArrayList<Parcelable> listParcelable) {
        DisplayListFragment fragment = new DisplayListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putInt(type, listType);

        //Toast.makeText(androidPlayer.getAppContext(),song.getTitle() , Toast.LENGTH_SHORT).show();
        args.putParcelableArrayList(ARG_LIST_TO_DISPLAY,listParcelable);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            listType = getArguments().getInt(type);
           specificListToDisplay = getArguments().getParcelableArrayList(ARG_LIST_TO_DISPLAY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album_list, container, false);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            switch (listType) {
                case 0:
                    recyclerView.setAdapter(new MySongRecyclerViewAdapter(mListener,null));
                break;
                case  1:
                    recyclerView.setAdapter(new MyAlbumRecyclerViewAdapter(mListener,null));
                break;
                case  2:
                    recyclerView.setAdapter(new MyArtistRecyclerViewAdapter(mListener));
                break;
                case 3:
                    ArrayList<Song> songToSend = (ArrayList<Song>) specificListToDisplay;
                    //Toast.makeText(context,String.valueOf(specificListToDisplay.size()),Toast.LENGTH_LONG).show();
                    recyclerView.setAdapter(new MySongRecyclerViewAdapter(mListener,specificListToDisplay));
                break;
                case 4:
                    ArrayList<Album> albumToSend = (ArrayList<Album>) specificListToDisplay;
                    recyclerView.setAdapter(new MyAlbumRecyclerViewAdapter(mListener,albumToSend));
                    break;
                default:
                    recyclerView.setAdapter(new MySongRecyclerViewAdapter(mListener,null));
            }
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Object o);
    }
}
