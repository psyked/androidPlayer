package com.example.psyke.lecteurmusique.modele;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by psyke on 11/11/2016.
 */

public class Playlist implements MediaPlayer.OnCompletionListener
{
    private static Context context;
    public String name;
    private ArrayList<Song> songs;
    private int current = 0;
    private boolean random = false;
    private MediaPlayer currentMedia = null;
    private ArrayList<Integer> indexRandom;

    public int index()
    {
        if(random)
        {
            return indexRandom.get(current);
        }
        else return current;
    }
    public static void addPlaylist(Playlist pl, SharedPreferences shps)
    {
        SharedPreferences.Editor prefsEditor = shps.edit();
        Gson gson = new Gson();
        String json = gson.toJson(pl);
        prefsEditor.putString("PLAYLIST." + pl.name, json);
        prefsEditor.commit();
    }
    public static ArrayList<Playlist> getPlaylists(SharedPreferences shps)
    {
        ArrayList<Playlist> plsArray = new ArrayList<Playlist>();
        Map<String , ?> prefs = shps.getAll();
        for ( Map.Entry<String, ?> entry: prefs.entrySet())
        {
            if(entry.getKey().contains("PLAYLIST."))
            {
                Gson gson = new Gson();
                plsArray.add(gson.fromJson((String)entry.getValue(), Playlist.class));
            }
        }
        return plsArray;
    }
    public static Playlist getPlaylist(SharedPreferences shps, String namePl)
    {
        String json = shps.getString("PLAYLIST." + namePl, "");
        if(json.equals("")) return null;
        Gson gson = new Gson();
        return gson.fromJson(json, Playlist.class);
    }
    public Playlist(String name, Context c)
    {
        context = c;
        this.name = name;
        songs = new ArrayList<Song>();
    }
    public Playlist(String name,Context c, ArrayList<Song> sgs)
    {
        context = c;
        this.name = name;
        songs = sgs;
    }
    public ArrayList<Song> getSongs(){return songs;}
    public boolean isRandom(){return random;}
    public Song getCurrentSong(){return songs.get(index());}
    public int getCurrentIndex(){return index();};
    public Song getSong(int x){return songs.get(x);}
    public void addSong(Song s)
    {
        songs.add(s);
    }
    public void addSongAndPLay(Song s){songs.add(s); playSong(songs.size()-1);}
    public int getIndexOf(Song s){
        int i = 0 ;
        for (Song song:songs) {
            if (song.isEqual(s))
                return i;
            i++;
        }
        return -1;
    }
    public void removeAllSongs(){
        songs =new ArrayList<>();
    }
    public void delSong(int pos)
    {
        songs.remove(pos);
        indexRandom.remove(indexRandom.indexOf(pos));
        if(current == 0)current = songs.size()-1;
        else current --;
    }
    public void readNext()
    {
        Log.d("readNext", "start next song");
        if(current >= songs.size()-1)current = 0;
        else current++;
        stopSong();
        if(currentMedia != null)playSong(index());
    }
    public void readPrevious()
    {
        Log.d("readPrevious","restart previous song");
        if(current == 0)current = songs.size()-1;
        else current--;
        stopSong();
        if(currentMedia != null)playSong(index());
    }
    public void stopSong()
    {
        Log.d("stopSong", "stop current song");
        if(currentMedia != null) currentMedia.stop();
    }
    public void pauseSong(){
        Log.d("pauseSong", "pause");
        if(currentMedia != null && currentMedia.isPlaying()){
            currentMedia.pause();
        }
    }
    public void playSong()
    {
        Log.d("playSong", "index : " + index());
        if(currentMedia != null)
        {
            Log.d("playSong", "song not null");
            if(currentMedia.isPlaying())
            {
                return;
            }
            Log.d("playSong", "song not playing");
            currentMedia.start();
        }
        else
        {
            Log.d("playSong", "create new Song");
            currentMedia = MediaPlayer.create(context, songs.get(index()).getURI());
            currentMedia.setOnCompletionListener(this);
            currentMedia.start();
        }
    }
    public void playSong(int pos)
    {
        current = pos;
        Log.d("playSong", "index : " + index());
        if(currentMedia != null)
        {Log.d("playSong", "song not null");
            if(! currentMedia.isPlaying())
            {
                Log.d("playSong", "song not playing");
                currentMedia = MediaPlayer.create(context, songs.get(pos).getURI());
                currentMedia.setOnCompletionListener(this);
                currentMedia.start();
            }
        }
        else
        {
            Log.d("playSong", "create new Song");
            currentMedia = MediaPlayer.create(context, songs.get(pos).getURI());
            currentMedia.setOnCompletionListener(this);
            currentMedia.start();
        }
    }
    public void randomiseSongs()
    {
        random = true;
        indexRandom = new ArrayList<Integer>();
        int i = 0;
        for (Song song : songs)
        {
               indexRandom.add(i++);
        }
        Collections.shuffle(indexRandom, new Random(System.nanoTime()));
    }
    public void unRandomiseSongs()
    {
        random = false;
    }
    public void addSongs(List<Song> ls)
    {
        for (Song s : ls)
        {
            if(songs.indexOf(s) >= 0) continue;
            songs.add(s);
        }
    }
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        Log.d("onCompletion", mp.toString());
        currentMedia = null;
        readNext();
    }

}
