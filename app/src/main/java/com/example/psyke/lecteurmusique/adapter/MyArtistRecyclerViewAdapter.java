package com.example.psyke.lecteurmusique.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.psyke.lecteurmusique.modele.Artist;
import com.example.psyke.lecteurmusique.DataColector;
import com.example.psyke.lecteurmusique.R;
import com.example.psyke.lecteurmusique.fragment.DisplayListFragment;

import java.util.List;

/**
 * Created by arnau on 17/11/2016.
 */

public class MyArtistRecyclerViewAdapter  extends RecyclerView.Adapter<MyArtistRecyclerViewAdapter.ViewHolder>{
    private DataColector dataColector= new DataColector();
    private final List<Artist> mValues;
    private final DisplayListFragment.OnListFragmentInteractionListener mListener;

    public MyArtistRecyclerViewAdapter(DisplayListFragment.OnListFragmentInteractionListener listener) {
        mValues = (List)dataColector.getArtistList();
        mListener = listener;
    }

    @Override
    public MyArtistRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_album, parent, false);
        return new MyArtistRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyArtistRecyclerViewAdapter.ViewHolder holder, int position) {
        String subtitles = mValues.get(position).getAlbumsNumber()+" Albums - "+mValues.get(position).getTotalSongsNumber()+" Morceaux";

        holder.mArtist = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getName());
        holder.mContentView.setText(subtitles);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mArtist);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Artist mArtist;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.textView2);
            mContentView = (TextView) view.findViewById(R.id.textView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
